# Ace Editor for Magento

By default, Magento Admin use an old version of a WYSIWYG editor called TinyMCE.
This is not ideal, because it's used to edit HTML fragments instead of documents. Because you are editing fragments, you can't really use the WYSIWYG editor because it will generate really ugly code.

This module replaces the CMS (+CSS and XML) <textarea /> elements in the back-office of Magento by instances of [Ace Editor](http://ace.c9.io/) when it's possible.

## Settings

The settings of this extension can be found in Advanced > Ace Editor

The default settings should cover the mose useful elements, but you can add custom elements by specifying the <textarea /> ID attribute.