<?php

class Lepape_AceEditor_Block_Editor extends Mage_Adminhtml_Block_Template
{
    const XML_PATH_PREFIX = 'ace_editor/';

    /**
     * Helper to get a setting value from the config and encode it as a JS string
     * @param $key
     * @return string
     */
    public function getSetting($key)
    {
        $value = Mage::getStoreConfig(self::XML_PATH_PREFIX . $key);
        if (is_numeric($value)) {
            return $value;
        }
        return Mage::helper('core')->jsonEncode($value);
    }

    public function getEditableTextAreas()
    {
        $allEditableTextAreas = array_merge($this->_getEditableAreas('html'), $this->_getEditableAreas('css'), $this->_getEditableAreas('xml'));
        return Mage::helper('core')->jsonEncode($allEditableTextAreas);
    }

    protected function _getEditableAreas($mode)
    {
        $areas = array();

        /* Get the base list of textarea IDs to add the editor to (chosen from the multiselect). */
        $editableAreas = explode(',', Mage::getStoreConfig('ace_editor/general/editable_' . $mode . '_areas'));
        foreach ($editableAreas as $editableArea) {
            $areas[] = array('type' => $mode, 'id' => $editableArea);
        }

        /* Add on any additional custom textarea IDs (specified using the Magento 'grid' control). */
        $editableAreasCustom = @unserialize(Mage::getStoreConfig('ace_editor/general/editable_' . $mode . '_areas_custom'));
        if (!empty($editableAreasCustom)) {
            foreach ($editableAreasCustom as $editableArea) {
                foreach($editableArea as $area) {
                    $areas[] = array('type' => $mode, 'id' => $area);
                }
            }
        }
        return $areas;
    }
}