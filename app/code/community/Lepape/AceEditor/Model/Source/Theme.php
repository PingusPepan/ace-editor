<?php

class Lepape_AceEditor_Model_Source_Theme
{

    public function toOptionArray()
    {
        return array(
            array('value' => 'ambiance', 'label' => 'Ambiance'),
            array('value' => 'chaos', 'label' => 'Chaos'),
            array('value' => 'chrome', 'label' => 'Chrome'),
            array('value' => 'clouds', 'label' => 'Clouds'),
            array('value' => 'clouds_midnight', 'label' => 'Clouds Midnight'),
            array('value' => 'cobalt', 'label' => 'Cobalt'),
            array('value' => 'crimson_editor', 'label' => 'Crimson Editor'),
            array('value' => 'dawn', 'label' => 'Dawn'),
            array('value' => 'dreamweaver', 'label' => 'Dreamweaver'),
            array('value' => 'eclipse', 'label' => 'Eclipse'),
            array('value' => 'github', 'label' => 'Github'),
            array('value' => 'idle_fingers', 'label' => 'Idle Fingers'),
            array('value' => 'kr', 'label' => 'KR'),
            array('value' => 'merbivore', 'label' => 'Merbivore'),
            array('value' => 'merbivore_soft', 'label' => 'Merbivore Soft'),
            array('value' => 'mono_industrial', 'label' => 'Mono Industrial'),
            array('value' => 'monokai', 'label' => 'Monokai'),
            array('value' => 'pastel_on_dark', 'label' => 'Pastel on Dark'),
            array('value' => 'solarized_dark', 'label' => 'Solarized Dark'),
            array('value' => 'solarized_light', 'label' => 'Solarized Light'),
            array('value' => 'terminal', 'label' => 'Terminal'),
            array('value' => 'textmate', 'label' => 'Textmate'),
            array('value' => 'tomorrow', 'label' => 'Tomorrow'),
            array('value' => 'tomorrow_night_blue', 'label' => 'Tomorrow Night Blue'),
            array('value' => 'tomorrow_night_bright', 'label' => 'Tomorrow Night Bright'),
            array('value' => 'tomorrow_night_eighties', 'label' => 'Tomorrow Night Eighties'),
            array('value' => 'tomorrow_night', 'label' => 'Tomorrow Night'),
            array('value' => 'twilight', 'label' => 'Twilight'),
            array('value' => 'vibrant_ink', 'label' => 'Vibrant Ink'),
            array('value' => 'xcode', 'label' => 'Xcode'),
        );
    }
}