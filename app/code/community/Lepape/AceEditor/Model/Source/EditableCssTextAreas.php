<?php

class Lepape_AceEditor_Model_Source_EditableCssTextAreas
{
    public function toOptionArray()
    {
        $helper = Mage::helper('ace_editor');
        return array(
            array('value' => '#template_styles', 'label' => $helper->__('Template Styles')),
        );
    }
}

