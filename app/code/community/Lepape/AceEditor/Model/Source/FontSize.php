<?php

class Lepape_AceEditor_Model_Source_FontSize
{

    public function toOptionArray()
    {
        return array(
            array('value' => '8px', 'label' => '8px'),
            array('value' => '10px', 'label' => '10px'),
            array('value' => '12px', 'label' => '12px'),
            array('value' => '14px', 'label' => '14px'),
            array('value' => '16px', 'label' => '16px')
        );
    }
}